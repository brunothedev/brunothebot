# brunothebot

brunothebot is a bot made for stream.mattkc.com

## Configuration

The configuration is pretty simple, just a basic JSON file

```
{
    "server": "wss://server.mattkc.com:2002",
    "admin": "brunothebot",
    "token": "..."
}
```

## Commands

```
>help
```

Gets to this page

```
>repo
```

Links the repository link

```
>whychatkc
```

Tells why to use chatkc

```
>lolmaoxd
```

The funni

```
>hello
```

Says hello to you!

```
>joke
```

Says a joke
