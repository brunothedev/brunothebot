package config

import (
    "os"
    "log"
    "encoding/json"
)

type Config struct {
    Admin  string `json:"admin"` // Bot administrator
    Server string `json:"server"` // Server link
    Token  string `json:"token"` // Token auth
}

func LoadConfig(filepath string) Config { // Loads config
    var conf Config

    configFile, err := os.ReadFile(filepath)
    if err != nil {
        log.Fatal(err)
    }

    err = json.Unmarshal(configFile, &conf)
    if err != nil {
        log.Fatal(err)
    }

    if conf.Token == "" {
        log.Fatal("Please put your token on your config file")
    }

    return conf
}
