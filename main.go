package main

import (
    "fmt"
    "os"
    "log"
    "encoding/json"
    "strings"
    "math/rand"

    "gitlab.com/brunothedev/brunothebot/config"

    "golang.org/x/net/websocket"
)

const ORIGIN = "http://localhost/"

func main() {

    confHome := os.Getenv("XDG_CONFIG_HOME")
    if confHome == "" {
        confHome = "~/.config"
    }
    
    conf := config.LoadConfig(confHome + "/brunothebot/config.json")

    ws, err := websocket.Dial(conf.Server, "", ORIGIN)
    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("Using this token: ", conf.Token)
    checkUserStatus(ws, conf.Token)    
    mainLoop(ws, conf.Token)
}

func sendSocketMessage(ws *websocket.Conn, msg []byte) { // Send a message over the socket
    _, err := ws.Write(msg)
    if err != nil {
	    log.Fatal(err)
    }
}

func mainLoop(ws *websocket.Conn, token string) { // Main loop
    var received = make([]byte, 512)
    var n int
    var err error
    var receivedJson map[string]interface{}
    var jokes map[int]string
    jokes = make(map[int]string)

    // Declaring all funni jokes
    jokes[0] = "Why vegans are bad at math, because they can't use the cow-culador #1"
    jokes[1] = "A tester walk to a bar, bar a to walk tester A, walk tester A to a bar #2"

    for true {
        n, err = ws.Read(received)        
        if err != nil {
            log.Fatal(err)
        }
        if string(received) != "" {
            err = json.Unmarshal(received[:n], &receivedJson)
            fmt.Println(receivedJson)
            if receivedJson["type"] == "chat" {
                msg := strings.Fields(fmt.Sprint(receivedJson["data"].(map[string]interface{})["message"]))
                msgId := receivedJson["data"].(map[string]interface{})["id"].(float64)
                msgAuthor := receivedJson["data"].(map[string]interface{})["author"].(string)

                switch msg[0] {
                case "&gt;repo":
                    sendMessage(ws, token, "I'm hosted at https://gitlab.com/brunothedev/brunothebot", msgId)
                case "&gt;whychatkc":
                    sendMessage(ws, token, "Because youtube chat suckz", msgId)
                case "&gt;lolmaoxd":
                    sendMessage(ws, token, "hello, me is YEP , if u gib me moni fo fi bobux, u gonna be dis GIGACHAD baldiDance spongeDance , if u don, then dis LETSGOOO ReallyMad MikeBruh Chatting kcFail kcSmile kcSmug", msgId)
                case "&gt;hello":
                    sendMessage(ws, token, "Hello, @" + msgAuthor + "!", msgId)
                case "&gt;joke":
                    sendMessage(ws, token, jokes[rand.Intn(2)], msgId)
                case "&gt;help":
                    sendMessage(ws, token, "My commands are at: https://gitlab.com/brunothedev/brunothebot#commands", msgId)
                }
            }
        }
    }
}

func sendMessage(ws *websocket.Conn, token string, msg string, reply float64) {
    message := map[string]interface{} {
        "type": "message",
        "data": map[string]interface{} {
            "text": msg,
            "reply" : reply,
        },
        "auth": "google",
        "token": token,
    }
    
    json, err := json.Marshal(message)
    if err != nil {
        log.Fatal(err)
    }

    sendSocketMessage(ws, json)
    fmt.Println(string(json))
}

func checkUserStatus(ws *websocket.Conn, token string) { // Checking the user status
    status := map[string]interface{} {
        "type": "status",
        "data": map[string]interface{} {},
        "auth": "google",
        "token": token,
    }
    
    json, err := json.Marshal(status)
    if err != nil {
        log.Fatal(err)
    }

    sendSocketMessage(ws, json)
    fmt.Println(string(json))
}
